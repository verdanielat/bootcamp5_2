import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  // You can add global styles to this file, and also import other style files
  'import': '"~bootstrap/dist/css/bootstrap.min.css"',
  '*': {
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'background': {
    'height': [{ 'unit': 'px', 'value': 300 }]
  },
  'img': {
    'fontFamily': 'Cambria, Cochin, Georgia, Times, Times New Roman, serif'
  }
});
