import { Component, OnInit } from '@angular/core';
import { APIService } from '../services/api.service';
@Component({
  selector: 'app-event-master',
  templateUrl: './event-master.component.html',
  styleUrls: ['./event-master.component.css']
})
export class EventMasterComponent implements OnInit {

  constructor(private api:APIService) { }
  eventList:object[];

  event_name:string="";
  date:string="";
  location:string="";
  ticket_total:string="";
  ticket_price:string="";

  ngOnInit() {
  }
  
  add(){
    this.api.addEvent({
      "event_name":this.event_name,
      "date":this.date,
      "location":this.location,
      "ticket_total":this.ticket_total,
      "ticket_price":this.ticket_price
    }).subscribe(result => this.eventList = result );


     this.event_name = "";
    this.date = "";
    this.location = "";
    this.ticket_total ="";
    this.ticket_price = "";
}



   


  } 


