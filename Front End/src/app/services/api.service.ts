import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers , RequestOptions } from '@angular/http';                 
import { Observable } from 'rxjs/Rx';											
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';										

@Injectable()
export class APIService {

  constructor(private http:Http , private router:Router ) { }

eventList:object[];

  getEventList(){
  			return this.http
			  .get('http://localhost:8000/api/eventList')
  			 .map(result => result.json())
			.catch(error => Observable.throw(error.json().error)||"Server Error");
  }


  addEvent(obj:Object) { 
  	let body = JSON.stringify(obj);
  	let headers = new Headers({ "Content-type" : "application/json" });
  	let options = new RequestOptions({ headers : headers });

  	return this.http.post('http://localhost:8000/api/eventList/add', body, options)
    .map(result => result.json());
  	// .subscribe(
  	// 	result => {}
  	// 		localStorage.setItem('token',result.json().token);
  	// 		this.router.navigate(['/user']);
  	// 	},
  	// 	err => {
  	// 		localStorage.removeItem('token');
  	// 		this.router.navigate(['/register']);
  	// 	}
  	// 	);
  }
  buyTicket(obj: Object){
    let body = JSON.stringify(obj); 
    let headers = new Headers({ "Content-Type" : "application/json" });
    let options = new RequestOptions({ headers : headers });
    return this.http.post('http://localhost:8000/api/eventList/buy', body, options)
    .map(result => result.json());



  }




}