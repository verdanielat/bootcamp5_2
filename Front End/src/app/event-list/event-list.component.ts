import { Component, OnInit } from '@angular/core';
import { APIService } from '../services/api.service';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit {


  event_name:string="";
  date:string="";
  location:string="";
  ticket_price:string="";

  name:string="";
  phone:string="";
  email:string="";

constructor(private api:APIService) { }

eventList:object[];
  
  ngOnInit() { 
  		this.api.getEventList().subscribe(result => this.eventList = result);
  
    
  }

  buy(){
    this.api.buyTicket({
      "name":this.name,
      "phone":this.phone,
      "email":this.email
    }).subscribe(result => this.api.eventList = result )
  }

  






  // removeUser(id:number){
  //   this.api.removeUser(id)
  //           .subscribe(result => this.eventList = result);
  //           console.log(id);
  // }



}
